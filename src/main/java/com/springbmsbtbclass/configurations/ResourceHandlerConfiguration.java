package com.springbmsbtbclass.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@PropertySource("classpath:/bms_prop.properties")
public class ResourceHandlerConfiguration implements WebMvcConfigurer {

    @Value("${file.client.path}")
    String CLIENT_PATH;

    @Value("${file.server.path}")
    String SERVER_PATH;

    @Description("Mapping resource for file uploading to file system location")
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(CLIENT_PATH + "**").addResourceLocations("file://" + SERVER_PATH);




        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
    }

}
