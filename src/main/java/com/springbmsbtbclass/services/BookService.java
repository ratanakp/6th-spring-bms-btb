package com.springbmsbtbclass.services;


import com.springbmsbtbclass.models.Book;

import java.util.List;

public interface BookService {

    List<Book> getAll();

    Book findOne(Integer id);
}
