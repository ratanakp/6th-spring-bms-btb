package com.springbmsbtbclass.services;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UploadFileService {

    public String singleFileUpload(MultipartFile file, String folder);


    public List<String> multipleFilesUpload(List<MultipartFile> files, String folder);

    public String upload(MultipartFile file, String folder);

    public String upload(MultipartFile file);

    public List<String> upload(List<MultipartFile> files);

    public List<String> upload(List<MultipartFile> files, String folder);

}
