package com.springbmsbtbclass.services.uploads;

import com.springbmsbtbclass.services.UploadFileService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Description;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@PropertySource("classpath:/bms_prop.properties")
public class UploadFileServiceImpl implements UploadFileService {

    @Value("${file.client.path}")
    private String CLIENT_PATH;

    @Value("${file.server.path}")
    private String SERVER_PATH;

    @Description("Single file upload")
    public String singleFileUpload(MultipartFile file, String folder) {

        String server_path = "", client_path = "";

        server_path = this.SERVER_PATH;
        client_path = this.CLIENT_PATH;

        if (file.isEmpty()) {
            return "Error, Please choose file to upload!";
        }

        if (folder != null && !folder.isEmpty()) {
            System.out.println("meme");
            server_path = this.SERVER_PATH + folder;
            client_path = this.CLIENT_PATH + folder;
        }

        File path = new File(server_path);

        if (!path.exists())
            path.mkdir();

        String filename = file.getOriginalFilename();

        String extension = filename.substring(filename.lastIndexOf(".") + 1);

        String new_filename = UUID.randomUUID() + "." + extension;

        try {
            Files.copy(file.getInputStream(), Paths.get(server_path, new_filename));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return client_path + new_filename;
    }


    @Description("Multiple file upload")
    public List<String> multipleFilesUpload(List<MultipartFile> files, String folder) {
        List<String> filenames = new ArrayList<>();

        // Simple foreach
        for (MultipartFile file :
                files) {
            filenames.add(this.singleFileUpload(file, folder));

        }

        // Using lambda expression
        /*files.forEach(file -> {
            filenames.add(this.singleFileUpload(file, folder));
        });*/
        return filenames;
    }


    public String upload(MultipartFile file, String folder) {
        return this.singleFileUpload(file, folder);
    }

    public String upload(MultipartFile file) {
        return this.singleFileUpload(file, null);
    }

    public List<String> upload(List<MultipartFile> files) {
        return this.multipleFilesUpload(files, null);
    }

    public List<String> upload(List<MultipartFile> files, String folder) {
        return this.multipleFilesUpload(files, folder);
    }


}
